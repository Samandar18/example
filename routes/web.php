<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/admin/delete/{id}', 'AdminController@delete')->middleware('auth')->name('delete');
Route::get('/admin/edit/{id}', 'AdminController@edit')->middleware('auth')->name('edit');
Route::post('/delete', 'DeleteController@delete')->name('delete');
Route::post('/admin/edit/saqlash/{id}', 'AdminController@saqlash')->name('saqlash');
Route::post('/admin/profile/saveprofile/{id}', 'AdminController@saveprofile')->name('saveprofile');
Route::get('/save','AdminController@save')->name('save');
Route::get('/add','AdminController@add')->name('add');
Route::get('/profile','AdminController@profile')->name('profile');


Route::get('/tak', function() 
{
    $factory = Faker\Factory::create();
    for ( $i=0; $i<=400; $i++){
    DB::insert("INSERT INTO students (name,email,guruh,vazifasi) 
VALUES (?,?,?,?)", [$factory->name, $factory->unique()->email,'103-19','C++ bilan ishlash']);
}
});