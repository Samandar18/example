@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-center">
        <div class="col-md-8">
          <form action="save" method="get">
            <div class="form-group">
              <label for="exampleInputEmail1">FIO</label>
              <input type="text" name="name"class="form-control">
              <small id="emailHelp" class="form-text text-muted">
              Bu sizning bazadagi nomingiz</small>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Email</label>
              <input type="email" name="email" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Guruh</label>
              <input type="text" name="guruh" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Vazifasi</label>
              <input type="text" name="vazifasi" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
      </div>
    </div>
</div>
@endsection